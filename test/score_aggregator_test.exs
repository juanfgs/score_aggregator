defmodule ScoreAggregatorTest do
  use ExUnit.Case

  setup do
    %{
      cards: [
        %{name: "Player A", team: "Team X", score: 10},
        %{name: "Player B", team: "Team Y", score: 20},
        %{name: "Player C", team: "Team X", score: 15},
        %{name: "Player D", team: "Team Z", score: 5}
      ]
    }
  end

  describe "aggregate_team_score/2" do
    test "it returns the total score for all cards belonging to that team", %{cards: cards} do
      assert ScoreAggregator.aggregate_team_score(cards, "Team X") == 25
    end
  end
end
