defmodule ScoreAggregator.TradingCards.TradingCardTest do
  use ExUnit.Case, async: true
  alias ScoreAggregator.TradingCards.TradingCard

  setup do
    # For this module we use custom testing data. 
    %{
      cards: [
        %{"name" => "Lionel Messi", "team" => "Inter Miami FC", "score" => 23},
        %{"name" => "Kylian Mbappé", "team" => "PSG", "score" => 20},
        %{"name" => "Julián Álvarez", "team" => "Manchester City", "score" => 14}
      ]
    }
  end

  describe "build/1" do
    test "it returns structs from a map with string keys", %{cards: cards} do
      assert [
               %TradingCard{name: "Lionel Messi", team: "Inter Miami FC", score: 23},
               %TradingCard{name: "Kylian Mbappé", team: "PSG", score: 20},
               %TradingCard{name: "Julián Álvarez", team: "Manchester City", score: 14}
             ] = cards |> Enum.map(&TradingCard.build/1)
    end
  end
end
