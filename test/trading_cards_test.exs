defmodule ScoreAggregator.TradingCardsTest do
  use ExUnit.Case, async: true
  alias ScoreAggregator.TradingCards
  alias ScoreAggregator.TradingCards.TradingCard

  setup do
    # For this module we use custom testing data. 
    %{
      cards: [
        %TradingCard{name: "Lionel Messi", team: "Inter Miami FC", score: 23},
        %TradingCard{name: "Kylian Mbappé", team: "PSG", score: 20},
        %TradingCard{name: "Julián Álvarez", team: "Manchester City", score: 14},
        %TradingCard{name: "Jordi Alba", team: "Inter Miami FC", score: 16},
        %TradingCard{name: "Erling Haaland", team: "Manchester City", score: 25},
        %TradingCard{name: "Ousmane Dembelé", team: "PSG", score: 12},
        %TradingCard{name: "Sergio Busquets", team: "Inter Miami FC", score: 18}
      ]
    }
  end

  describe "filter_by_team_name/2" do
    test "it filters the cards by team", %{cards: cards} do
      team_name = "Inter Miami FC"

      for card <- TradingCards.filter_by_team(cards, team_name) do
        assert card.team == team_name
      end
    end
  end

  describe "sum_scores/1" do
    test "it sums the scores of the given cards", %{cards: cards} do
      assert 128 == TradingCards.sum_scores(cards)
    end
  end
end
