defmodule ScoreAggregator.TradingCards.TradingCard do
  @moduledoc """
  This module represents a trading card with it's score and, player name and team the player belongs to 
  """

  @type t :: %__MODULE__{name: String.t(), team: String.t(), score: Integer.t()}

  defstruct [:name, :team, :score]

  @doc """
   Convenience function to build a struct from a map with string keys
   this is useful when consuming objects serialized from HTTP requests
  """
  @spec build(map()) :: t()
  def build(%{"name" => name, "team" => team, "score" => score}) do
    %__MODULE__{
      name: name,
      team: team,
      score: score
    }
  end
end
