defmodule ScoreAggregator.TradingCards do
  @moduledoc """
  This module provides convenience functions for managing Trading Cards
  it provides functions that are composable on an elixir pipeline and
  are as self explanatory as possible.
  """

  alias ScoreAggregator.TradingCards.TradingCard

  @doc """
  Given a List of TradingCard this function returns only the ones
  that belong to the specified team 
  """
  @spec filter_by_team([TradingCard.t()], String.t()) :: [TradingCard.t()]
  def filter_by_team(trading_cards, team) do
    trading_cards
    |> Enum.filter(&(&1.team == team))
  end

  @doc """
  Given a list of TradingCard this function returns the sum of their scores
  """
  @spec sum_scores([TradingCard.t()]) :: Integer.t()
  def sum_scores(trading_cards) do
    trading_cards
    |> Enum.reduce(0, fn card, acc -> acc + card.score end)
  end
end
