defmodule ScoreAggregator do
  @moduledoc """
  Documentation for `ScoreAggregator`.
  """

  alias ScoreAggregator.TradingCards

  @doc """
  Given a list of TradingCards.TradingCard,
  this function returns the total scores of a specific team
  """
  @spec aggregate_team_score([TradingCards.TradingCard.t()], String.t()) :: Integer.t()
  def aggregate_team_score(trading_cards, team) do
    trading_cards
    |> TradingCards.filter_by_team(team)
    |> TradingCards.sum_scores()
  end
end
