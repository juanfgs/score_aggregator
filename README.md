# ScoreAggregator

## Installation

Clone the repository. If running MIX_ENV=dev|test you need to fetch dependencies using 

```
   mix deps.get
```

to install credo. Otherwise just run 

```
   iex -S mix run
```

To test the function you might want to run

```elixir
	# Define test cards
	cards = [
	%{name: "Player A", team: "Team X", score: 10},
	%{name: "Player B", team: "Team Y", score: 20},
	%{name: "Player C", team: "Team X", score: 15},
	%{name: "Player D", team: "Team Z", score: 5}
	]

	ScoreAggregator.aggregate_team_score(cards, "Team X")
```



